import React, { Suspense } from "react";
import { StyleSheet, Dimensions, LogBox } from "react-native";
import { Provider } from "react-redux";
import store from "./src/redux/stores";
import RootNavigator from "./src/navigations/root.navigator";

const screenWidth = Dimensions.get("screen").width;
const screenHeight = Dimensions.get("screen").height;

LogBox.ignoreAllLogs();

export default function App() {
  return (
    <Suspense fallback={null}>
      <Provider store={store}>
        <RootNavigator />
      </Provider>
    </Suspense>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  container2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
