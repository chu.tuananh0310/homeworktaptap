module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    comments: true,
    plugins: [
      [
        "module-resolver",
        {
          alias: {
            "~": "./src",
          },
        },
      ],
      "react-native-reanimated/plugin",
    ],
  };
};
