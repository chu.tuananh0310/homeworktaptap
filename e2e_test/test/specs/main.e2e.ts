import MainPage from "../pageobjects/main.page";

import {
  findbyText,
  findbyClassName,
  findbyID,
  findbyTextButtonAndroid,
  handle_ScrollUp,
  handle_ScrollDown,
  handle_element,
  getRndInteger,
} from "../../support/functions";

describe("Test Main page", () => {
  it("Main Page open", async () => {
    await expect(MainPage.header_Tilte).toHaveText("mrbeast");
    let title_name = await (await MainPage.header_Tilte).getText();
    if (title_name === "mrbeast") {
      await MainPage.scrollPageUp();
      await MainPage.scrollPageDown();
    }

    await driver.pause(5000);
  });

  it("Click Item", async () => {
    await expect(MainPage.header_Tilte).toHaveText("mrbeast");

    await MainPage.random_Click();

    await expect(MainPage.header_Tilte).toHaveText("PostDetails");

    await MainPage.scrollPageUp();

    await driver.pause(5000);

    let a = null;
    do {
      console.log("a isExisting : ", a);
      await MainPage.btn_back_Click();
      await driver.pause(5000);
      a = await (await findbyText("Search for a name")).isExisting();
    } while (a);

    await expect(MainPage.header_Tilte).toHaveText("Search for a name");

    await driver.pause(5000);
  });

  it("Search ", async () => {
    await expect(MainPage.header_Tilte).toHaveText("Search for a name");

    await MainPage.gotimkiem_input();

    let text = "Search for a name";
    let el = await findbyText(text);
    await expect(el).toHaveText(text);

    el.click();

    await expect(MainPage.header_Tilte).toHaveText("Search for a name");

    await MainPage.gotimkiem_input();

    el = await MainPage.timkiem_input;
    await el.setValue("f");

    let kq = await findbyText("fenerbahce");
    await expect(kq).toHaveText("fenerbahce");

    await driver.touchAction({
      action: "tap",
      x: 878,
      y: 163,
    });
    await el.setValue("Tuan Anh");
    kq = await findbyText("Tuan Anh");
    await expect(kq).toHaveText("Tuan Anh");
    let scroll = await findbyClassName("android.widget.ScrollView");
    let item = scroll.$(`[text="Tuan Anh"]`);
    await item.click();
  });
});
