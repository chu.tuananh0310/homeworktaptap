import Page from "./page";
import {
  findbyID,
  findbyText,
  handle_ScrollDown,
  handle_ScrollUp,
  handle_element,
} from "../../support/functions";

class MainPage extends Page {
  /**
   * define selectors using getter methods
   */
  public get header_Tilte() {
    return findbyText("mrbeast");
  }

  public get timkiem_input() {
    return findbyText("f");
  }

  /**
   * a method to encapsule automation code to interact with the page
   * e.g. to login using username and password
   */

  public async random_Click() {
    await driver.touchAction({
      action: "tap",
      x: 150,
      y: 138,
    });
  }

  public async showPageList() {
    await (await this.header_Tilte).click();
  }

  public async gotimkiem_input() {
    await (await this.timkiem_input).click();
  }

  public async btn_back_Click() {
    await driver.touchAction({
      action: "tap",
      x: 93,
      y: 138,
    });
  }

  public async scrollPageUp() {
    await handle_ScrollUp(1);
  }

  public async scrollPageDown() {
    await handle_ScrollDown(1);
  }

  public async scrollToFirstPage() {
    await handle_ScrollDown(1, null, 100);
  }
}

export default new MainPage();
