const mainpage = ["./test/specs/main.e2e.ts"];
const hoithoaipage = ["./test/specs/hoithoai.e2e.ts"];
const testall = [[...mainpage, ...hoithoaipage]];

export const suiteslist = {
  main: mainpage,
  hoithoai: hoithoaipage,
  testall: testall,
};
