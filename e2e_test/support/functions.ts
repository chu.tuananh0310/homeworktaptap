const findbyText = async (text) => {
  return await $(`[text="${text}"]`);
  //   return await browser.react$("DemoImageWithCache", {
  //     props: { text: text },
  //   });
};
const findbyClassName = async (classname) => {
  return await $(`.${classname}`);
};

const findbyID = async (id) => {
  return await $(`id=${id}`);
};

const findbyTextButtonAndroid = async (text) => {
  const selector = `new UiSelector().text("${text}").className("android.widget.Button")`;
  return await $(`android=${selector}`);
};

const handle_ScrollUp = async (
  time?: number,
  delay?: number,
  duration?: number
) => {
  let dem = 0;
  while (dem < time) {
    await driver
      .action("pointer")
      .move({ x: 500, y: 1700, duration: 0 })
      .down({ button: 0 })
      .move({ x: 570, y: 700, duration: duration || 300, origin: "viewport" })
      .up({ button: 0 })
      .perform(false);
    await driver.pause(delay || 1500);
    dem++;
  }
};

const handle_ScrollDown = async (
  time?: number,
  delay?: number,
  duration?: number
) => {
  let dem = 0;
  while (dem < time) {
    await driver
      .action("pointer")
      .move({ x: 570, y: 750, duration: 0 })
      .down({ button: 0 })
      .move({ x: 500, y: 1700, duration: duration || 300, origin: "viewport" })
      .up({ button: 0 })
      .perform(false);
    await driver.pause(delay || 1500);
    dem++;
  }
};

const handle_element = async (
  by: "id" | "text",
  action: "click" | "scroll up" | "scroll down" | "have text",
  elevalue: string,
  compare_value?: string
) => {
  let element = null;
  switch (by) {
    case "text":
      element = await findbyText(elevalue);
      break;

    default:
      element = await findbyID(elevalue);
      break;
  }
  if (element) {
    switch (action) {
      case "click":
        await element.click();
        break;
      case "scroll up":
        break;
      case "scroll down":
        break;
      case "have text":
        await expect(element).toHaveText(compare_value);
        break;
      default:
        break;
    }

    await driver.pause(3000); // nghỉ 3s cho mỗi hành động
  }
  return element;
};

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export {
  findbyText,
  findbyClassName,
  findbyID,
  findbyTextButtonAndroid,
  handle_ScrollUp,
  handle_ScrollDown,
  handle_element,
  getRndInteger,
};
