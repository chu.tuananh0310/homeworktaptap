let accessToken = null;

let baseUrl = "https://instagram-scraper-api2.p.rapidapi.com";

const defaultOptions = {
  baseUrl: baseUrl,
};

class BaseService {
  constructor(options = defaultOptions) {
    this.baseUrl = options.baseUrl;
  }

  async postImage(endpoint, idProduct, objBody) {
    if (objBody && objBody.length > 0) {
      try {
        const access_token = await _getAccessToken();
        let body = new FormData();
        objBody.map((source) => {
          body.append("files", {
            uri: source.uri,
            name: source.uri.slice(source.uri.lastIndexOf("/") + 1),
            type: `image/${source.uri.substr(source.uri.lastIndexOf(".") + 1)}`,
          });
        });
        const response = await fetch(
          `${this.baseUrl}${"files/"}${endpoint}${`/${idProduct}/images`}`,
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${access_token}`,
            },
            body: body ? body : null,
            credentials: "include",
          }
        );
        if (!response.ok) {
          //error
        }

        return await response.json();
      } catch (error) {
        console.log("error PostImage", error);
      }
    } else {
      return true;
    }
  }

  get(endpoint) {
    return this.requestHttp("GET", endpoint);
  }

  post(endpoint, params) {
    return this.requestHttp("POST", endpoint, params);
  }

  upload(endpoint, params) {
    return this.requestHttp("POST", endpoint, params, headers);
  }

  put(endpoint, params) {
    return this.requestHttp("PUT", endpoint, params);
  }

  patch(endpoint, params) {
    return this.requestHttp("PATCH", endpoint, params);
  }

  delete(endpoint, params) {
    return this.requestHttp("DELETE", endpoint, params);
  }

  async requestHttp(method, endpoint, params, headers) {
    const url = this.baseUrl + endpoint;
    // const access_token = await _getAccessToken();
    const options = {
      method,
      headers: {
        // Accept: "application/json",
        // "Content-Type": "application/json",
        // Authorization: `Bearer ${access_token}`,
        "x-rapidapi-key": "9f79aac863mshee434e7ec4b2261p1917d9jsn73116e6b2fcb",
        "x-rapidapi-host": "instagram-scraper-api2.p.rapidapi.com",
      },
    };
    if (headers)
      options.headers["Content-Type"] =
        body instanceof FormData ? "multipart/form-data" : "application/json";
    if (params) options.body = JSON.stringify(params);

    try {
      const response = await fetch(url, options);
      const result = await response.text();

      if (!response.ok) {
        if (response.status === 403) {
          return {
            errors: `You dont have the permission`,
          };
        }
        if (response.status === 401) {
          return handle_logout && handle_logout();
          // return {
          //   errors: "Có lỗi xảy ra, Bạn vui lòng đăng xuất và đăng nhập lại",
          // };
        }
        const isExcludeError =
          response.status === 404 || response.status === 422;
        if (!isExcludeError) {
          // SentryError.captureMessage("error_call_api", response);
        }
        return false;
      }

      return JSON.parse(result);
    } catch (error) {
      console.log("error:", error);
    }
  }
}

const Api = new BaseService({ baseUrl: baseUrl });

export { Api };
