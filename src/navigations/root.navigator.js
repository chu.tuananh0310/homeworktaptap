import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import React from "react";
import { AppRoute } from "./app.routes";

import Main from "../screens/main";
import ItemDetails from "../screens/postdetails/postDetails";

const Stack = createStackNavigator();

const RootNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={AppRoute.HOME}>
        <Stack.Screen
          name={AppRoute.HOME}
          component={Main}
          options={{
            header: () => null,
          }}
        />
        {/* <Stack.Screen
          name={AppRoute.SEARCH}
          component={SearchView}
          options={
            {
              // header: () => null,
            }
          }
        /> */}

        <Stack.Screen
          name={AppRoute.POSTDETAILS}
          component={ItemDetails}
          options={{
            // header: () => null,
            presentation: "modal",
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default RootNavigator;
