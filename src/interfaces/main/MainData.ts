interface PostsReelsQuery {
  username_or_id_or_url: string;
  pagination_token?: string | null;
}

interface MainData {
  data: DataDetails;
  pagination_token: string;
}

interface DataDetails {
  count: number;
  items: PostsReelsItem[];
  user: UserInfo;
}

interface UserInfo {
  full_name: string;
  id: string;
  is_private: boolean;
  is_verified: boolean;
  profile_pic_id: string;
  profile_pic_url: string;
  username: string;
}

interface PostsReelsItem {
  boost_unavailable_identifier: any;
  boost_unavailable_reason: any;
  can_reshare: boolean;
  can_save: boolean;
  caption: Caption;
  caption_is_edited: boolean;
  carousel_media?: CarouselMedia[];
  carousel_media_count?: number;
  carousel_media_ids?: Array<number>;
  carousel_media_pending_post_count?: number;
  clips_tab_pinned_user_ids: Array<any>;
  coauthor_producers: Array<{
    full_name: string;
    id: string;
    is_private: boolean;
    is_verified: boolean;
    profile_pic_id: string;
    profile_pic_url: string;
    username: string;
  }>;
  code: string;
  comment_count: number;
  comment_inform_treatment: {
    action_type: any;
    should_have_inform_treatment: boolean;
    text: string;
    url: any;
  };
  comment_likes_enabled?: boolean;
  comment_threading_enabled: boolean;
  commerciality_status: string;
  deleted_reason: number;
  device_timestamp: number;
  fb_aggregated_comment_count: number;
  fb_aggregated_like_count: number;
  fb_user_tags: {
    in: Array<any>;
  };
  fbid: string;
  featured_products: Array<any>;
  filter_type: number;
  fundraiser_tag: {
    has_standalone_fundraiser: boolean;
  };
  has_liked: boolean;
  has_more_comments: boolean;
  has_privately_liked: boolean;
  has_shared_to_fb: number;
  id: string;
  ig_media_sharing_disabled: boolean;
  image_versions: {
    items: Array<{
      height: number;
      url: string;
      width: number;
    }>;
    additional_items?: {
      first_frame: {
        height: number;
        url: string;
        width: number;
      };
      igtv_first_frame: {
        height: number;
        url: string;
        width: number;
      };
      smart_frame: any;
    };
    scrubber_spritesheet_info_candidates?: {
      default: {
        file_size_kb: number;
        max_thumbnails_per_sprite: number;
        rendered_width: number;
        sprite_height: number;
        sprite_urls: Array<string>;
        sprite_width: number;
        thumbnail_duration: number;
        thumbnail_height: number;
        thumbnail_width: number;
        thumbnails_per_row: number;
        total_thumbnail_num_per_sprite: number;
        video_length: number;
      };
    };
    smart_thumbnail_enabled?: boolean;
  };
  inline_composer_display_condition: string;
  inline_composer_imp_trigger_time: number;
  integrity_review_decision: string;
  invited_coauthor_producers: Array<any>;
  is_auto_created: boolean;
  is_comments_gif_composer_enabled: boolean;
  is_cutout_sticker_allowed: boolean;
  is_eligible_for_media_note_recs_nux: boolean;
  is_in_profile_grid: boolean;
  is_open_to_public_submission: boolean;
  is_organic_product_tagging_eligible: boolean;
  is_paid_partnership: boolean;
  is_pinned: boolean;
  is_post_live_clips_media: boolean;
  is_quiet_post: boolean;
  is_reshare_of_text_post_app_media_in_ig: boolean;
  is_reuse_allowed: boolean;
  is_tagged_media_shared_to_viewer_profile_grid: boolean;
  is_unified_video: boolean;
  is_video: boolean;
  like_and_view_counts_disabled: boolean;
  like_count: number;
  location: any;
  max_num_visible_preview_comments: number;
  media_name: string;
  media_type: number;
  mezql_token: string;
  music_metadata?: {
    audio_canonical_id: string;
    audio_type: any;
    music_info: any;
    original_sound_info: any;
    pinned_media_ids: any;
  };
  next_max_id?: number;
  open_carousel_submission_state?: string;
  original_height: number;
  original_width: number;
  owner: {
    account_badges: Array<any>;
    can_see_quiet_post_attribution: boolean;
    fan_club_info: {
      autosave_to_exclusive_highlight: any;
      connected_member_count: any;
      fan_club_id: any;
      fan_club_name: any;
      fan_consideration_page_revamp_eligiblity: any;
      has_enough_subscribers_for_ssc: any;
      is_fan_club_gifting_eligible: any;
      is_fan_club_referral_eligible: any;
      subscriber_count: any;
    };
    fbid_v2: number;
    feed_post_reshare_disabled: boolean;
    full_name: string;
    has_anonymous_profile_picture: boolean;
    id: string;
    is_favorite: boolean;
    is_private: boolean;
    is_unpublished: boolean;
    is_verified: boolean;
    profile_pic_id: string;
    profile_pic_url: string;
    show_account_transparency_details: boolean;
    third_party_downloads_enabled: number;
    transparency_product_enabled: boolean;
    username: string;
  };
  preview_comments: Array<{
    comment_like_count: number;
    content_type: string;
    created_at: number;
    created_at_utc: number;
    did_report_as_spam: boolean;
    has_liked_comment: boolean;
    hashtags: Array<any>;
    id: string;
    is_covered: boolean;
    is_created_by_media_owner?: boolean;
    is_pinned: boolean;
    is_ranked_comment: boolean;
    mentions: Array<string>;
    share_enabled: boolean;
    text: string;
    type: number;
    user: {
      fbid_v2: number;
      full_name: string;
      id: string;
      is_private: boolean;
      is_verified: boolean;
      profile_pic_id: string;
      profile_pic_url: string;
      username: string;
    };
    user_id: string;
  }>;
  product_suggestions: Array<any>;
  product_type: string;
  share_count_disabled: boolean;
  sharing_friction_info: {
    bloks_app_url: any;
    sharing_friction_payload: any;
    should_have_sharing_friction: boolean;
  };
  shop_routing_user_id: any;
  should_show_author_pog_for_tagged_media_shared_to_profile_grid: boolean;
  social_context: Array<any>;
  sponsor_tags: Array<any>;
  subscribe_cta_visible: boolean;
  tagged_users: Array<{
    categories?: Array<string>;
    duration_in_video_in_sec: any;
    position: Array<number>;
    show_category_of_user: boolean;
    start_time_in_video_in_sec: any;
    user: {
      full_name: string;
      id: string;
      is_private: boolean;
      is_verified: boolean;
      profile_pic_id?: string;
      profile_pic_url: string;
      username: string;
    };
    x: number;
    y: number;
  }>;
  taken_at: number;
  taken_at_ts: number;
  thumbnail_url: string;
  timeline_pinned_user_ids?: Array<number>;
  top_likers: Array<any>;
  user: {
    account_badges: Array<any>;
    eligible_for_text_app_activation_badge: boolean;
    fan_club_info: {
      autosave_to_exclusive_highlight: any;
      connected_member_count: any;
      fan_club_id: any;
      fan_club_name: any;
      fan_consideration_page_revamp_eligiblity: any;
      has_enough_subscribers_for_ssc: any;
      is_fan_club_gifting_eligible: any;
      is_fan_club_referral_eligible: any;
      subscriber_count: any;
    };
    fbid_v2: number;
    feed_post_reshare_disabled: boolean;
    full_name: string;
    has_anonymous_profile_picture: boolean;
    id: string;
    is_favorite: boolean;
    is_private: boolean;
    is_unpublished: boolean;
    is_verified: boolean;
    profile_pic_id: string;
    profile_pic_url: string;
    show_account_transparency_details: boolean;
    third_party_downloads_enabled: number;
    transparency_product_enabled: boolean;
    username: string;
  };
  are_remixes_crosspostable?: boolean;
  clips_metadata?: {
    achievements_info: {
      num_earned_achievements: any;
      show_achievements: boolean;
    };
    additional_audio_info: {
      additional_audio_username: any;
      audio_reattribution_info: {
        should_allow_restore: boolean;
      };
    };
    asset_recommendation_info: any;
    audio_canonical_id: string;
    audio_ranking_info: {
      best_audio_cluster_id: string;
    };
    audio_type: string;
    branded_content_tag_info: {
      can_add_tag: boolean;
    };
    breaking_content_info: any;
    breaking_creator_info: any;
    challenge_info: any;
    clips_creation_entry_point: string;
    content_appreciation_info: {
      enabled: boolean;
      entry_point_container: any;
    };
    contextual_highlight_info: any;
    cutout_sticker_info: Array<any>;
    disable_use_in_clips_client_cache: boolean;
    external_media_info: any;
    featured_label: any;
    is_fan_club_promo_video: boolean;
    is_public_chat_welcome_video: boolean;
    is_shared_to_fb: boolean;
    mashup_info: {
      can_toggle_mashups_allowed: boolean;
      formatted_mashups_count: any;
      has_been_mashed_up: boolean;
      has_nonmimicable_additional_audio: boolean;
      is_creator_requesting_mashup: boolean;
      is_light_weight_check: boolean;
      is_pivot_page_available: boolean;
      mashup_type: any;
      mashups_allowed: boolean;
      non_privacy_filtered_mashups_media_count: number;
      original_media: any;
      privacy_filtered_mashups_media_count: any;
    };
    merchandising_pill_info: any;
    music_info: any;
    nux_info: any;
    original_sound_info: {
      allow_creator_to_rename: boolean;
      audio_asset_id: number;
      audio_filter_infos: Array<any>;
      audio_parts: Array<any>;
      audio_parts_by_filter: Array<any>;
      can_remix_be_shared_to_fb: boolean;
      can_remix_be_shared_to_fb_expansion: boolean;
      consumption_info: {
        display_media_id: any;
        is_bookmarked: boolean;
        is_trending_in_clips: boolean;
        should_mute_audio_reason: string;
        should_mute_audio_reason_type: any;
      };
      duration_in_ms: number;
      formatted_clips_media_count: any;
      hide_remixing: boolean;
      ig_artist: {
        full_name: string;
        id: string;
        is_private: boolean;
        is_verified: boolean;
        profile_pic_id: string;
        profile_pic_url: string;
        username: string;
      };
      is_audio_automatically_attributed: boolean;
      is_eligible_for_audio_effects: boolean;
      is_explicit: boolean;
      is_original_audio_download_eligible: boolean;
      is_reuse_disabled: boolean;
      is_xpost_from_fb: boolean;
      oa_owner_is_music_artist: boolean;
      original_audio_subtype: string;
      original_audio_title: string;
      original_media_id: number;
      progressive_download_url: string;
      should_mute_audio: boolean;
      time_created: number;
      trend_rank: any;
      xpost_fb_creator_info: any;
    };
    originality_info: any;
    professional_clips_upsell_type: number;
    reels_on_the_rise_info: any;
    reusable_text_attribute_string: any;
    reusable_text_info: any;
    shopping_info: any;
    show_achievements: boolean;
    show_tips: any;
    template_info: any;
    viewer_interaction_settings: any;
  };
  collab_follow_button_info?: {
    is_owner_in_author_exp: boolean;
    show_follow_button: boolean;
  };
  commerce_integrity_review_decision?: string;
  creator_viewer_insights?: Array<any>;
  fb_like_count?: number;
  fb_play_count?: number;
  has_audio?: boolean;
  is_artist_pick?: boolean;
  is_dash_eligible?: number;
  is_third_party_downloads_eligible?: boolean;
  media_cropping_info?: {
    square_crop: {
      crop_bottom: number;
      crop_left: number;
      crop_right: number;
      crop_top: number;
    };
  };
  number_of_qualities?: number;
  play_count?: number;
  share_count?: number;
  should_open_collab_bottomsheet_on_facepile_tap?: boolean;
  video_codec?: string;
  video_duration?: number;
  video_subtitles_confidence?: number;
  video_subtitles_locale?: string;
  video_url?: string;
  video_versions?: Array<{
    height: number;
    id: string;
    type: number;
    url: string;
    width: number;
  }>;
  mashup_info?: {
    can_toggle_mashups_allowed: boolean;
    formatted_mashups_count: any;
    has_been_mashed_up: boolean;
    has_nonmimicable_additional_audio: boolean;
    is_creator_requesting_mashup: boolean;
    is_light_weight_check: boolean;
    is_pivot_page_available: boolean;
    mashup_type: any;
    mashups_allowed: boolean;
    non_privacy_filtered_mashups_media_count: number;
    original_media: any;
    privacy_filtered_mashups_media_count: any;
  };
}

// interface Caption {
//   content_type: string;
//   created_at: number;
//   created_at_utc: number;
//   did_report_as_spam: boolean;
//   hashtags: string[];
//   id: string;
//   is_covered: boolean;
//   is_ranked_comment: boolean;
//   mentions: string[];
//   private_reply_status: number;
//   share_enabled: boolean;
//   text: string;
//   type: number;
//   user: {
//     account_badges: string;
//     fan_club_info: {
//       autosave_to_exclusive_highlight?: any;
//       connected_member_count?: any;
//       fan_club_id?: any;
//       fan_club_name?: any;
//       fan_consideration_page_revamp_eligiblity?: any;
//       has_enough_subscribers_for_ssc?: any;
//       is_fan_club_gifting_eligible?: any;
//       is_fan_club_referral_eligible?: any;
//       subscriber_count?: any;
//     };
//     fbid_v2: string;
//     feed_post_reshare_disabled: boolean;
//     full_name: string;
//     has_anonymous_profile_picture: boolean;
//     id: string;
//     is_favorite: boolean;
//     is_private: boolean;
//     is_unpublished: boolean;
//     is_verified: boolean;
//     profile_pic_id: string;
//     profile_pic_url: string;
//     show_account_transparency_details: boolean;
//     third_party_downloads_enabled: number;
//     transparency_product_enabled: boolean;
//     username: string;
//   };
//   user_id: string;
// }

interface Hashtags {}

interface CarouselMedia {
  carousel_parent_id: string;
  commerciality_status: string;
  explore_pivot_grid: boolean;
  fb_user_tags: {
    in: Array<any>;
  };
  featured_products: Array<any>;
  id: string;
  image_versions: {
    items: Array<{
      height: number;
      url: string;
      width: number;
    }>;
    scrubber_spritesheet_info_candidates?: {
      default: {
        file_size_kb: number;
        max_thumbnails_per_sprite: number;
        rendered_width: number;
        sprite_height: number;
        sprite_urls: Array<string>;
        sprite_width: number;
        thumbnail_duration: number;
        thumbnail_height: number;
        thumbnail_width: number;
        thumbnails_per_row: number;
        total_thumbnail_num_per_sprite: number;
        video_length: number;
      };
    };
  };
  is_video: boolean;
  location: any;
  media_name: string;
  media_type: number;
  original_height: number;
  original_width: number;
  product_suggestions: Array<any>;
  product_type: string;
  sharing_friction_info: {
    bloks_app_url: any;
    sharing_friction_payload: any;
    should_have_sharing_friction: boolean;
  };
  shop_routing_user_id: any;
  sponsor_tags: Array<any>;
  tagged_users: Array<{
    categories?: Array<string>;
    duration_in_video_in_sec: any;
    position: Array<number>;
    show_category_of_user: boolean;
    start_time_in_video_in_sec: any;
    user: {
      full_name: string;
      id: string;
      is_private: boolean;
      is_verified: boolean;
      profile_pic_id?: string;
      profile_pic_url: string;
      username: string;
    };
    x: number;
    y: number;
  }>;
  taken_at: number;
  taken_at_ts: number;
  thumbnail_url: string;
  has_audio?: boolean;
  is_dash_eligible?: number;
  number_of_qualities?: number;
  video_codec?: string;
  video_duration?: number;
  video_url?: string;
  video_versions?: Array<{
    height: number;
    id: string;
    type: number;
    url: string;
    width: number;
  }>;
}

interface Caption {
  content_type: string;
  created_at: number;
  created_at_utc: number;
  did_report_as_spam: boolean;
  hashtags: Array<any>;
  id: string;
  is_covered: boolean;
  is_ranked_comment: boolean;
  mentions: Array<string>;
  private_reply_status: number;
  share_enabled: boolean;
  text: string;
  type: number;
  user: {
    fbid_v2: number;
    full_name: string;
    id: string;
    is_private: boolean;
    is_verified: boolean;
    profile_pic_id: string;
    profile_pic_url: string;
    username: string;
  };
  user_id: string;
}
