import {NativeModules} from 'react-native';
const {AndroidKeyboardAdjust} = NativeModules;

export const setAdjustNothing = () => {
  AndroidKeyboardAdjust.setAdjustNothing();
};

export const setAdjustPan = () => {
  AndroidKeyboardAdjust.setAdjustPan();
};

export const setAdjustResize = () => {
  AndroidKeyboardAdjust.setAdjustResize();
};

export const setAdjustUnspecified = () => {
  AndroidKeyboardAdjust.setAdjustUnspecified();
};

export const setAlwaysHidden = () => {
  AndroidKeyboardAdjust.setAlwaysHidden();
};

export const setAlwaysVisible = () => {
  AndroidKeyboardAdjust.setAlwaysVisible();
};

export const setVisible = () => {
  AndroidKeyboardAdjust.setVisible();
};

export const setHidden = () => {
  AndroidKeyboardAdjust.setHidden();
};

export const setUnchanged = () => {
  AndroidKeyboardAdjust.setUnchanged();
};

export const getSoftInputMode = () => {
  return new Promise<boolean>((resolve) => {
    AndroidKeyboardAdjust.getSoftInputMode((isInstalled: any) => {
      resolve(isInstalled);
    });
  });
};

export const setSoftInputMode = (mode: string) => {
  AndroidKeyboardAdjust.setSoftInputMode(mode);
};
