import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
  wrap: {
    flexWrap: 'wrap'
  },
  textXs: {
    fontSize: 12
  },
  textMd: {
    fontSize:20
  },
  textBold: {
    fontWeight: '600'
  },
  textThin: {
    fontWeight: '300'
  },
  upper: {
    textTransform: 'uppercase'
  },
  relative: {
    position: 'relative',
  },
  absolute: {
    position: 'absolute',
  },
  container: {
    paddingHorizontal: 16
  },
  flexRow: {
    flexDirection: 'row'
  },
  flex1: {
    flex: 1
  },
  flex2: {
    flex: 2
  },
  flex3: {
    flex: 3
  },
  flex4: {
    flex: 4
  },
  justifyCenter: {
    justifyContent: 'center'
  },
  justifyBetween: {
    justifyContent: 'space-between'
  },
  justifyAround: {
    justifyContent: 'space-around'
  },
  justifyEnd: {
    justifyContent: 'flex-end'
  },
  itemsStart: {
    alignItems: 'flex-start'
  },
  itemsCenter: {
    alignItems: 'center'
  },
  itemsEnd: {
    alignItems: 'flex-end'
  },
  selfCenter: {
    alignSelf: 'center'
  },
  selfEnd: {
    alignSelf: 'flex-end'
  },
  row: {
    flexDirection: 'row'
  },
  rowReverse: {
    flexDirection: 'row-reverse'
  },
  wFull: {
    width: '100%'
  },
  resizeCover: {
    resizeMode: 'cover'
  },
  bgWhite: {
    backgroundColor: 'white'
  },
  textWhite: {
    color: 'white'
  },
  bgGray: {
    backgroundColor: '#EEF1F7'
  },
  bgBlack: {
    backgroundColor: '#000000'
  },
  bgBlue: {
    backgroundColor: '#3BB6FF'
  },
  pb0: {
    paddingBottom: 0
  },
  pb8: {
    paddingBottom: 8
  },
  pb16: {
    paddingBottom: 16
  },
  pb24: {
    paddingBottom: 24
  },
  pr16: {
    paddingRight: 16
  },
  p4: {
    padding: 4
  },
  p8: {
    padding: 8
  },
  p16: {
    padding: 16
  },
  p24: {
    padding: 24
  },
  // padding vertial
  py8: {
    paddingVertical: 8
  },
  py16: {
    paddingVertical: 16
  },
  py24: {
    paddingVertical: 24
  },
  // padding horizotial
  px8: {
    paddingHorizontal: 8
  },
  px16: {
    paddingHorizontal: 16
  },
  px24: {
    paddingHorizontal: 24
  },
  // margin-horizontal
  mx4: {
    marginHorizontal: 4
  },
  mx8: {
    marginHorizontal: 8
  },
  mx12: {
    marginHorizontal: 12
  },
  mx16: {
    marginHorizontal: 16
  },
  mx24: {
    marginHorizontal: 24
  },
  mx32: {
    marginHorizontal: 32
  },
  mx40: {
    marginHorizontal: 40
  },
  my16: {
    marginVertical: 16
  },
  my24: {
    marginVertical: 24
  },
  my32: {
    marginVertical: 32
  },
  my40: {
    marginVertical: 40
  },
  // margin-bottom
  mb0: {
    marginBottom: 0
  },
  mb8: {
    marginBottom: 8
  },
  mb16: {
    marginBottom: 16
  },
  mb24: {
    marginBottom: 24
  },
  mb32: {
    marginBottom: 32
  },
  mb40: {
    marginBottom: 40
  },
  // margin-top
  mt0: {
    marginTop: 0
  },
  mt8: {
    marginTop: 8
  },
  mt12: {
    marginTop: 12
  },
  mt16: {
    marginTop: 16
  },
  mt24: {
    marginTop: 24
  },
  mt32: {
    marginTop: 32
  },
  mt40: {
    marginTop: 40
  },
  // margin-left
  ml8: {
    marginLeft: 8
  },
  ml12: {
    marginLeft: 12
  },
  ml24: {
    marginLeft: 24
  },
  ml16: {
    marginLeft: 16
  },
  // margin-right
  mr8: {
    marginRight: 8
  },
  mr24: {
    marginRight: 24
  },
  mr16: {
    marginRight: 16
  },
  my8: {
    marginVertical: 8
  },
  textCenter: {
    textAlign: 'center'
  },
  hFull: {
    height: '100%'
  },
  inset: {
    left: 0,
    top: 0,
    right: 0,
    bottom: 0
  },
  round8: {
    borderRadius: 8
  },
  round12: {
    borderRadius: 12
  },
  overflowHidden: {
    overflow: 'hidden'
  },
})

export default styles