import { Api } from "../../services/base.service";

export const searchUser = (params) => {
  return Api.get(`/v1/search_users?search_query=${params}`);
};
