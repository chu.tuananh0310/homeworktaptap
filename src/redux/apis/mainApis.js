import { Api } from "../../services/base.service";

export const getPostsReelsList = (params) => {
  return Api.get(
    `/v1.2/posts?username_or_id_or_url=${params.username_or_id_or_url}
      ${
        params.pagination_token
          ? `&pagination_token=${params.pagination_token}`
          : ""
      }`
  );
};
