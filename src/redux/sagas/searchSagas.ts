import { put, takeLatest, call } from "redux-saga/effects";
import { SearchConstants } from "../contants";
import { searchUser } from "../apis/searchApis";

export function* watchSearchDataApi() {
  yield takeLatest(SearchConstants.SEARCH_DATA, searchData);
}

function* searchData({ payload }: { payload: string }) {
  try {
    const res: SearchData = yield call(searchUser, payload);
    console.log("searchData res : ", res);

    yield put({
      type: SearchConstants.SEARCH_DATA_SUCCESS,
      payload: { data: res },
    });
  } catch (error) {
    yield put({
      type: SearchConstants.SEARCH_DATA_FAILURE,
      payload: { error: error.message },
    });
  }
}
