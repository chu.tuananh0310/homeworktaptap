import { put, takeLatest, call } from "redux-saga/effects";
import { MainConstants } from "../contants";
import { getPostsReelsList } from "../apis/mainApis";

export function* watchGetMainDataApi() {
  yield takeLatest(MainConstants.GET_MAIN_DATA, getMainData);
}

function* getMainData({ payload }: { payload: PostsReelsQuery }) {
  try {
    const res: MainData = yield call(getPostsReelsList, payload);

    yield put({
      type: MainConstants.GET_MAIN_DATA_SUCCESS,
      payload: { data: res },
    });
  } catch (error) {
    yield put({
      type: MainConstants.GET_MAIN_DATA_FAILURE,
      payload: { error: error.message },
    });
  }
}
