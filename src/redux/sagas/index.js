import { all, fork } from "redux-saga/effects";
import { watchGetMainDataApi } from "./mainSagas";
import { watchSearchDataApi } from "./searchSagas";

export default function* rootSaga() {
  yield all([fork(watchGetMainDataApi), fork(watchSearchDataApi)]);
}
