import { combineReducers } from "redux";

import { mainReducer } from "./mainReducer";
import { searchReducer } from "./searchReducer";

const rootReducer = combineReducers({
  mainReducer,
  searchReducer,
});

export default rootReducer;
