import { SearchConstants } from "../contants";

interface SearchState {
  searchData: SearchData;
}

const initialState: SearchState = {
  searchData: null,
};

export const searchReducer = (
  state = initialState,
  action: { type: any; payload: { data: SearchData } }
) => {
  switch (action.type) {
    case SearchConstants.SEARCH_DATA:
      return { ...state };
    case SearchConstants.SEARCH_DATA_SUCCESS:
      let { data: newData } = action.payload.data;
      console.log("SEARCH_DATA_SUCCESS newData : ", newData);
      return {
        ...state,
        searchData: newData,
      };
    case SearchConstants.SEARCH_DATA_FAILURE:
      return { ...state, searchData: null };
    default:
      return state;
  }
};
