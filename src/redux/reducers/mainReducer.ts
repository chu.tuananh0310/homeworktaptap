import { MainConstants } from "../contants";

interface MainState {
  maindata: MainData;
}

const initialState: MainState = {
  maindata: null,
};

export const mainReducer = (
  state = initialState,
  action: { type: any; payload: { data: MainData } }
) => {
  switch (action.type) {
    case MainConstants.GET_MAIN_DATA:
      return { ...state };
    case MainConstants.GET_MAIN_DATA_SUCCESS:
      let { data: newData } = action.payload;
      return {
        ...state,
        maindata: !state?.maindata
          ? newData
          : {
              data: {
                count:
                  Number(state?.maindata?.data?.count || 0) +
                  Number(newData?.data?.count),
                items: [
                  ...state?.maindata?.data?.items,
                  ...newData?.data?.items,
                ],
                user: newData?.data?.user,
              },
              pagination_token: newData?.pagination_token,
            },
      };
    case MainConstants.GET_MAIN_DATA_FAILURE:
      return { ...state };
    default:
      return state;
  }
};
