import { MainConstants } from "../contants";

export const getMainData = (query: PostsReelsQuery) => {
  return {
    type: MainConstants.GET_MAIN_DATA,
    payload: query,
  };
};

export const getMainDataSuccess = (data: MainData) => {
  return {
    type: MainConstants.GET_MAIN_DATA_SUCCESS,
    payload: {
      data,
    },
  };
};

export const getMainDataError = (error: any) => {
  return {
    type: MainConstants.GET_MAIN_DATA_FAILURE,
    payload: {
      error,
    },
  };
};
