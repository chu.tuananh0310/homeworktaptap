import { SearchConstants } from "../contants";

export const searchData = (query: string) => {
  return {
    type: SearchConstants.SEARCH_DATA,
    payload: query,
  };
};

export const searchDataSuccess = (data: UserInfo[]) => {
  return {
    type: SearchConstants.SEARCH_DATA_SUCCESS,
    payload: {
      data,
    },
  };
};

export const searchDataError = (error: any) => {
  return {
    type: SearchConstants.SEARCH_DATA_FAILURE,
    payload: {
      error,
    },
  };
};
