import React, { useEffect, useRef, useState } from "react";
import { View, StyleSheet, Dimensions, Platform } from "react-native";
import { Image } from "expo-image";
import Carousel from "react-native-reanimated-carousel";
import CarouselProgress from "./CarouselProgress";
import { scaledSize } from "../utils/responsivesize";
import { useVideoPlayer, VideoView } from "expo-video";

const SLIDER_WIDTH = Dimensions.get("window").width;
const SLIDER_HEIGHT = Dimensions.get("window").height;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);

const CarouselList = ({ data }: { data: CarouselMedia[] }): JSX.Element => {
  const [indexImage, setIndexImage] = React.useState<number>(0);

  const Item = ({ item, index }) => {
    return item?.is_video ? (
      <VideoShow video_url={item?.video_url} />
    ) : (
      <Image
        style={styles.image}
        source={item.thumbnail_url}
        contentFit="cover"
      />
    );
  };
  return (
    <View style={styles.container}>
      <Carousel
        width={SLIDER_WIDTH}
        loop={data.length > 1}
        autoPlay={false}
        onSnapToItem={(index) => setIndexImage(index)}
        data={data}
        renderItem={Item}
      />
      <View
        style={{
          position: "absolute",
          bottom: scaledSize(20),
          left: 0,
          right: 0,
        }}
      >
        <CarouselProgress
          step={indexImage}
          size={data.length}
          style={{ marginTop: 16 }}
        />
      </View>
    </View>
  );
};

export default CarouselList;

const VideoShow = ({ video_url }: { video_url: string }) => {
  const ref = useRef(null);
  const [isPlaying, setIsPlaying] = useState(true);
  const player = useVideoPlayer(video_url);

  useEffect(() => {
    const subscription = player.addListener("playingChange", (isPlaying) => {
      setIsPlaying(isPlaying);
    });

    return () => {
      subscription.remove();
    };
  }, [player]);

  return (
    <VideoView
      ref={ref}
      style={styles.image}
      player={player}
      allowsFullscreen
      allowsPictureInPicture
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: ITEM_WIDTH,
    height: SLIDER_HEIGHT * 0.6,
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
