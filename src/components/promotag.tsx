import React from "react";
import { View, StyleSheet, Text, ImageBackground } from "react-native";
import { Image } from "expo-image";

const PromoTag = ({ promo, isNotNumber }): JSX.Element => {
  return (
    <ImageBackground
      style={{
        width: 50,
        height: 24,
        justifyContent: "center",
        alignItems: "center",
      }}
      source={require("../assets/promo.png")}
      resizeMode="stretch"
    >
      <Text
        style={{
          color: "#fff",
          fontSize: 12,
          fontWeight: "700",
          fontStyle: "normal",
          textTransform: isNotNumber ? "uppercase" : "none",
        }}
      >
        {isNotNumber ? "" : "-"}
        {promo ? promo : "Promo"}
      </Text>
    </ImageBackground>
  );
};

export default PromoTag;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5F7FA",
    paddingTop: 5,
  },
  body: {
    width: "100%",
    backgroundColor: "#fff",
    paddingVertical: 20,
    paddingLeft: 20,
  },
  image: {
    width: "60%",
    height: 24,
    borderRadius: 8,
  },
  timetext: {
    backgroundColor: "#FF7926",
    color: "#fff",
    fontSize: 14,
    fontWeight: "700",
    lineHeight: 22,
    paddingHorizontal: 4,
    borderRadius: 4,
    height: 22,
  },
  timesym: {
    color: "#FF7926",
    fontSize: 14,
    fontWeight: "700",
    lineHeight: 22,
    paddingHorizontal: 2,
  },
  des: {
    fontSize: 14,
    fontWeight: "700",
    lineHeight: 22,
    paddingLeft: 8,
  },
});
