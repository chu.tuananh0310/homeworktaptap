import { useTheme } from "@emotion/react";
import React from "react";
import { View } from "react-native";
import styles from "../common/styles";
import type { ViewProps } from "react-native";

interface Props extends ViewProps {
  size: number;
  step: number;
  width?: number;
}

export default function CarouselProgress({
  size,
  step,
  style,
  width = 48,
  ...props
}: Props) {
  const theme = useTheme();

  return (
    <View style={[styles.itemsCenter, style]} {...props}>
      <View
        style={[
          styles.flexRow,
          {
            backgroundColor: "#BCECFF",
            borderRadius: 3,
            width,
            overflow: "hidden",
          },
        ]}
      >
        {[...new Array(size)].map((_, i) => (
          <View
            key={i}
            style={{
              flex: 1,
              height: 6,
              borderRadius: 3,
              ...(step === i && {
                backgroundColor: "gray",
              }),
            }}
          />
        ))}
      </View>
    </View>
  );
}
