import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  FlatList,
  Text,
  SafeAreaView,
  ScrollView,
  StatusBar,
} from "react-native";
import { scaledSize } from "../../utils/responsivesize";
import { useRoute } from "@react-navigation/native";
import UserInfo from "./useInfo";
import CarouselList from "../../components/CarouselList";
import ReadMore from "react-native-read-more-text";

import LikeIcon from "../../assets/svg/like.svg";
import CommentIcon from "../../assets/svg/comment.svg";
import SaveIcon from "../../assets/svg/save.svg";
import SharePostIcon from "../../assets/svg/sharepost.svg";
import BaChamIcon from "../../assets/svg/bacham.svg";
import VerifiedIcon from "../../assets/svg/verified.svg";

const SLIDER_WIDTH = Dimensions.get("window").width;
const SLIDER_HEIGHT = Dimensions.get("window").height;

const icon_size = scaledSize(25);

const blurhash =
  "|rF?hV%2WCj[ayj[a|j[az_NaeWBj@ayfRayfQfQM{M|azj[azf6fQfQfQIpWXofj[ayj[j[fQayWCoeoeaya}j[ayfQa{oLj?j[WVj[ayayj[fQoff7azayj[ayj[j[ayofayayayj[fQj[ayayj[ayfjj[j[ayjuayj[";

const ItemDetails = () => {
  const route = useRoute();
  const { item } = route.params;
  console.log("ItemDetails item: ", item);
  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View>
            <UserInfo userInfo={item.user} />
            <CarouselList data={item?.carousel_media} />
            {/* <View
            style={{
              flexDirection: "row",
              width: SLIDER_WIDTH,
              height: SLIDER_HEIGHT * 0.9,
              backgroundColor: "red",
            }}
          >

            {item?.carousel_media.map((i: CarouselMedia, idx: any) => {
              console.log("aaa i: ", i);
              i.is_video ? (
                <Image
                  key={idx}
                  style={{ width: "100%", height: "100%" }}
                  source={item.thumbnail_url}
                  placeholder={{ blurhash }}
                  contentFit="contain"
                  transition={1000}
                />
              ) : (
                <Image
                  key={idx}
                  style={{ width: "100%", height: "100%" }}
                  source={i.thumbnail_url}
                  placeholder={{ blurhash }}
                  contentFit="contain"
                  transition={1000}
                />
              );
            })}
          </View> */}
            <ControlView />
            <LikeView likes={item?.like_count} />
            <CaptionView caption={item?.caption} user={item?.user} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default ItemDetails;

const ControlView = () => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        margin: scaledSize(10),
      }}
    >
      <View style={{ flexDirection: "row" }}>
        <View
          style={{
            width: icon_size,
            height: icon_size,
          }}
        >
          <LikeIcon width="100%" height="100%" />
        </View>
        <View
          style={{
            width: icon_size,
            height: icon_size,
            marginLeft: scaledSize(10),
          }}
        >
          <CommentIcon width="100%" height="100%" />
        </View>
        <View
          style={{
            width: icon_size,
            height: icon_size,
            marginLeft: scaledSize(10),
          }}
        >
          <SharePostIcon width="100%" height="100%" />
        </View>
      </View>
      <View>
        <View
          style={{
            width: icon_size,
            height: icon_size,
            marginLeft: scaledSize(10),
          }}
        >
          <SaveIcon width="100%" height="100%" />
        </View>
      </View>
    </View>
  );
};

const LikeView = ({ likes }: { likes: number }) => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        margin: scaledSize(10),
      }}
    >
      <Text style={{ fontSize: scaledSize(14) }}>
        {Number(likes).toLocaleString("en")} likes
      </Text>
    </View>
  );
};

const CaptionView = ({ caption, user }: { caption: Caption; user: any }) => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        margin: scaledSize(10),
      }}
    >
      <ReadMore
        numberOfLines={3}
        renderTruncatedFooter={_renderTruncatedFooter}
        renderRevealedFooter={_renderRevealedFooter}
        onReady={_handleTextReady}
      >
        <Text style={{ fontSize: scaledSize(16) }}>{user.username}</Text>
        {user.is_verified && (
          <View
            style={{
              width: scaledSize(16),
              height: scaledSize(16),
              marginLeft: scaledSize(10),
            }}
          >
            <VerifiedIcon width="100%" height="100%" />
          </View>
        )}
        {caption.text}
      </ReadMore>
    </View>
  );
};

const _renderTruncatedFooter = (handlePress: Function) => {
  return (
    <Text style={{ color: "blue", marginTop: 5 }} onPress={handlePress}>
      Read more
    </Text>
  );
};

const _renderRevealedFooter = (handlePress: Function) => {
  return (
    <Text style={{ color: "blue", marginTop: 5 }} onPress={handlePress}>
      Show less
    </Text>
  );
};

const _handleTextReady = () => {
  // ...
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    marginTop: StatusBar.currentHeight || 0,
  },
  image: {
    width: SLIDER_WIDTH * 0.3,
    height: SLIDER_WIDTH * 0.3,
    borderRadius: 1000,
  },
});
