import React from "react";
import { View, StyleSheet, Dimensions, FlatList, Text } from "react-native";
import { Image } from "expo-image";
import { scaledSize } from "../../utils/responsivesize";
import { LinearGradient } from "expo-linear-gradient";

import VerifiedIcon from "../../assets/svg/verified.svg";

const SLIDER_WIDTH = Dimensions.get("window").width;
const SLIDER_HEIGHT = Dimensions.get("window").height;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);

const icon_size = scaledSize(25);

const blurhash =
  "|rF?hV%2WCj[ayj[a|j[az_NaeWBj@ayfRayfQfQM{M|azj[azf6fQfQfQIpWXofj[ayj[j[fQayWCoeoeaya}j[ayfQa{oLj?j[WVj[ayayj[fQoff7azayj[ayj[j[ayofayayayj[fQj[ayayj[ayfjj[j[ayjuayj[";

const UserInfo = ({ userInfo }: { userInfo: UserInfo }) => {
  return (
    <View
      style={{
        paddingHorizontal: scaledSize(10),
        paddingVertical: scaledSize(10),
      }}
    >
      <View style={{ flexDirection: "row" }}>
        <LinearGradient
          colors={[
            "rgba(255, 0, 0, 1)",
            "rgba(255, 154, 0, 1)",
            "rgba(208, 222, 33, 1)",
            "rgba(79, 220, 74, 1)",
            "rgba(63, 218, 216, 1)",
            "rgba(47, 201, 226, 1)",
            "rgba(28, 127, 238, 1)",
            "rgba(95, 21, 242, 1)",
            "rgba(186, 12, 248, 1)",
            "rgba(251, 7, 217, 1)",
            "rgba(255, 0, 0, 1)",
          ]}
          style={{ padding: 5, borderRadius: 1000 }}
        >
          <Image
            style={styles.image}
            source={userInfo.profile_pic_url}
            placeholder={{ blurhash }}
            contentFit="cover"
            transition={1000}
          />
        </LinearGradient>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            paddingLeft: scaledSize(10),
            paddingTop: scaledSize(10),
          }}
        >
          <Text style={{ fontSize: scaledSize(20) }}>{userInfo.username}</Text>
          {userInfo.is_verified && (
            <View
              style={{
                width: icon_size,
                height: icon_size,
                marginLeft: scaledSize(10),
              }}
            >
              <VerifiedIcon width="100%" height="100%" />
            </View>
          )}
        </View>
      </View>
    </View>
  );
};

export default UserInfo;

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  image: {
    width: SLIDER_WIDTH * 0.1,
    height: SLIDER_WIDTH * 0.1,
    borderRadius: 1000,
  },
});
