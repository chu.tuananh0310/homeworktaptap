import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import ListUser from "./listUser";

const SLIDER_WIDTH = Dimensions.get("window").width;

const SearchViewList = ({ data }: { data: UserInfo[] }): JSX.Element => {
  return (
    <View style={styles.container}>
      <ListUser items={data} />
    </View>
  );
};

export default SearchViewList;

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  image: {
    width: SLIDER_WIDTH * 0.3,
    height: SLIDER_WIDTH * 0.3,
    borderRadius: 1000,
  },
});
