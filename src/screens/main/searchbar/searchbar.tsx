import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import { debounce } from "lodash";
import { useDispatch } from "react-redux";
import { searchData } from "../../../redux/actions";

const SearchBar = (): JSX.Element => {
  const [searchText, setSearchText] = useState("");
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    // const debounceSearch = debounce(() => {
    //   dispatch(searchData(searchText));
    // }, 150);
    // debounceSearch();
    dispatch(searchData(searchText));
  }, [searchText]);

  return (
    <View style={styles.container}>
      <View style={styles.container1}>
        <Image
          style={styles.image}
          source={require("../../../assets/images/search.png")}
        />
        <TextInput
          value={searchText}
          onChangeText={(val) => setSearchText(val)}
          placeholder={"Search for a name"}
          style={styles.text}
        />
      </View>
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    marginTop: 14,
    paddingHorizontal: 16,
  },
  container1: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F5F7FA",
    borderRadius: 16,
    paddingVertical: 4,
  },
  image: {
    width: 24,
    height: 24,
  },
  text: { fontSize: 14, fontWeight: "400", paddingLeft: 8, lineHeight: 20 },
});
