import { useNavigation } from "@react-navigation/native";
import React from "react";
import {
  StyleSheet,
  Dimensions,
  FlatList,
  TouchableOpacity,
  View,
  Text,
} from "react-native";
import { Image } from "expo-image";
import { scaledSize } from "../../../utils/responsivesize";

import VerifiedIcon from "../../../assets/svg/verified.svg";

const SLIDER_WIDTH = Dimensions.get("window").width;

const ListUser = ({ items }: { items: UserInfo[] }): JSX.Element => {
  console.log("ListUser items : ", items);
  return (
    <FlatList
      style={{ paddingTop: 12 }}
      horizontal={false}
      keyExtractor={(item) => item.id}
      data={items}
      renderItem={({ item }) => <Item key={item.id} item={item} />}
    />
  );
};

export default ListUser;

const Item = ({ item }: { item: UserInfo }) => {
  console.log("Item item : ", item);
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        margin: scaledSize(1),

        marginHorizontal: scaledSize(10),
      }}
    >
      <Image
        style={{
          width: scaledSize(60),
          height: scaledSize(60),
          borderRadius: 1000,
        }}
        source={item.profile_pic_url}
        contentFit="cover"
        transition={1000}
      />
      <View style={{ marginLeft: scaledSize(10) }}>
        <View style={{ flexDirection: "row" }}>
          <Text style={{ fontSize: scaledSize(18) }}>{item.username}</Text>
          {item.is_verified && (
            <View
              style={{
                width: scaledSize(14),
                height: scaledSize(14),
                marginLeft: scaledSize(10),
              }}
            >
              <VerifiedIcon width="100%" height="100%" />
            </View>
          )}
        </View>

        <Text style={{ color: "gray" }}>{item.full_name}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  image: {
    width: SLIDER_WIDTH * 0.3,
    height: SLIDER_WIDTH * 0.3,
    borderRadius: 1000,
  },
});
