import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  FlatList,
  Text,
  TouchableOpacity,
} from "react-native";
import { Image } from "expo-image";
import { scaledSize } from "../../../utils/responsivesize";
import { useNavigation } from "@react-navigation/native";
import { AppRoute } from "../../../navigations/app.routes";

const SLIDER_WIDTH = Dimensions.get("window").width;

const blurhash =
  "|rF?hV%2WCj[ayj[a|j[az_NaeWBj@ayfRayfQfQM{M|azj[azf6fQfQfQIpWXofj[ayj[j[fQayWCoeoeaya}j[ayfQa{oLj?j[WVj[ayayj[fQoff7azayj[ayj[j[ayofayayayj[fQj[ayayj[ayfjj[j[ayjuayj[";

const Item = ({ item }: { item: PostsReelsItem }) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(AppRoute.POSTDETAILS, { item: item });
      }}
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: scaledSize(1),
        backgroundColor: "red",
        height: SLIDER_WIDTH / 3,
      }}
    >
      <Image
        style={{ width: "100%", height: "100%" }}
        source={item.thumbnail_url}
        placeholder={{ blurhash }}
        contentFit="cover"
        transition={1000}
      />
    </TouchableOpacity>
  );
};

export default Item;

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  image: {
    width: SLIDER_WIDTH * 0.3,
    height: SLIDER_WIDTH * 0.3,
    borderRadius: 1000,
  },
});
