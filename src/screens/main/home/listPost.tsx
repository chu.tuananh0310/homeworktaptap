import React from "react";
import { StyleSheet, Dimensions, FlatList } from "react-native";
import Item from "./itemPost";

const SLIDER_WIDTH = Dimensions.get("window").width;

const ListPost = ({
  items,
  loadMore,
}: {
  items: PostsReelsItem[];
  loadMore: Function;
}): JSX.Element => {
  return (
    <FlatList
      numColumns={3}
      style={{ paddingTop: 12 }}
      horizontal={false}
      keyExtractor={(item) => item.id}
      data={items}
      renderItem={({ item }) => <Item key={item.id} item={item} />}
      initialNumToRender={5}
      onEndReachedThreshold={3}
      onEndReached={() => {
        loadMore && loadMore();
      }}
    />
  );
};

export default ListPost;

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  image: {
    width: SLIDER_WIDTH * 0.3,
    height: SLIDER_WIDTH * 0.3,
    borderRadius: 1000,
  },
});
