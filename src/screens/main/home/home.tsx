import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";

import UserInfo from "./useInfo";
import ListPost from "./listPost";

const SLIDER_WIDTH = Dimensions.get("window").width;

const Home = ({
  data,
  loadMore,
}: {
  data: DataDetails;
  loadMore: Function;
}): JSX.Element => {
  return (
    <View style={styles.container}>
      <UserInfo userInfo={data.user} />
      <ListPost items={data.items} loadMore={loadMore} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  image: {
    width: SLIDER_WIDTH * 0.3,
    height: SLIDER_WIDTH * 0.3,
    borderRadius: 1000,
  },
});
