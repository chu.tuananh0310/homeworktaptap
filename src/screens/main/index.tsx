import React, { useCallback, useEffect, useMemo } from "react";
import {
  View,
  StatusBar,
  StyleSheet,
  SafeAreaView,
  ScrollView,
} from "react-native";
import SearchBar from "./searchbar/searchbar";
import { useDispatch, useSelector } from "react-redux";
import { getMainData } from "../../redux/actions";
import Home from "./home/home";
import { searchReducer } from "../../redux/reducers/searchReducer";
import SearchViewList from "./searchbar/searchViewList";

const Main = () => {
  const dispatch = useDispatch();

  const mainData: MainData = useSelector((state) => state.mainReducer.maindata);
  const searchData: SearchData = useSelector(
    (state) => state.searchReducer.searchData
  );

  const loadData = async () => {
    const query: PostsReelsQuery = {
      username_or_id_or_url: "mrbeast",
      pagination_token: mainData?.pagination_token,
    };
    dispatch(getMainData(query));
  };

  useEffect(() => {
    loadData();
  }, []);

  const loadMore = () => {
    loadData();
  };

  const ContentView = useCallback(() => {
    if (searchData?.count > 0) {
      return <SearchViewList data={searchData?.items} />;
    }
    return (
      <View>
        {mainData && mainData.data && (
          <Home data={mainData.data} loadMore={loadMore} />
        )}
      </View>
    );
  }, [searchData]);

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <SearchBar />
        {/* <ScrollView> */}
        <ContentView />
        {/* </ScrollView> */}
      </SafeAreaView>
    </View>
  );
};

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    marginTop: StatusBar.currentHeight || 0,
  },
  list: {
    width: "100%",
    backgroundColor: "#000",
  },
  item: {
    aspectRatio: 1,
    width: "100%",
    flex: 1,
  },
  title: {
    fontSize: 32,
  },
  headerContainer: {
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#397af8",
    height: "auto",
    width: "100%",
    // paddingBottom: 50,
  },
  heading: {
    color: "white",
    fontSize: 22,
    fontWeight: "bold",
  },
});
