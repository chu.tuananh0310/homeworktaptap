export default {
  expo: {
    name: "TapTapTest",
    slug: "TapTapTest",
    version: "1.0.0",
    orientation: "portrait",
    icon: "./assets/icon.png",
    userInterfaceStyle: "light",
    splash: {
      image: "./assets/splash.png",
      resizeMode: "contain",
      backgroundColor: "#ffffff",
    },
    assetBundlePatterns: ["**/*"],
    ios: {
      supportsTablet: true,
      bundleIdentifier: "com.anh.chu.taptaptest",
      infoPlist: {
        NSBluetoothAlwaysUsageDescription:
          "taptaptest would like access to your bluetooth",
        NSCameraUsageDescription:
          "Access taptaptest use camera to scan barcode and add Product",
        NSPhotoLibraryAddUsageDescription:
          "taptaptest would like access to your photo gallery to add image product",
        NSPhotoLibraryUsageDescription:
          "taptaptest would like access to your photo gallery to add image product",
        PHPhotoLibraryPreventAutomaticLimitedAccessAlert:
          "taptaptest would like access to your photo gallery to add image product",
      },
    },
    android: {
      adaptiveIcon: {
        foregroundImage: "./assets/adaptive-icon.png",
        backgroundColor: "#ffffff",
      },
      package: "com.anh.chu.test.taptap",
      runtimeVersion: {
        policy: "sdkVersion",
      },
      permissions: [
        "VIBRATE",
        "WAKE_LOCK",
        "INTERNET",
        "SCHEDULE_EXACT_ALARM",
        "READ_EXTERNAL_STORAGE",
        "WRITE_EXTERNAL_STORAGE",
        "MEDIA_LIBRARY",
        "ACCESS_MEDIA_LOCATION",
        "READ_MEDIA_IMAGE",
        "READ_MEDIA_VIDEO",
      ],
    },
    web: {
      favicon: "./assets/favicon.png",
    },
  },
};
